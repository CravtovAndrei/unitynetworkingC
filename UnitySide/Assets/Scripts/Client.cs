﻿using System;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

class Client : MonoBehaviour
{
    void Start()
    {
        SendClientRequest();
    }

    static void SendClientRequest()
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 7000);
            Debug.Log("Client connected.");
            byte[] bytes = new byte[256];

            NetworkStream stream = client.GetStream();

            Debug.Log("Enter the request.");
            string request = "We are in Unity.";
            bytes = Encoding.ASCII.GetBytes(request);
            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
            Debug.Log("Request was sent.");

            byte[] bytesRead = new byte[256];
            int length = stream.Read(bytesRead, 0, bytesRead.Length);
            string answer = Encoding.ASCII.GetString(bytesRead, 0, length);

            Debug.Log($"Got the server answer: {answer}.");

            client.Close();
            Debug.Log("Client done it's work.");
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }
}

