﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SimpleServerClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                TcpListener serverSocket = new TcpListener(IPAddress.Any,7000);
                Console.WriteLine("Server started");

                serverSocket.Start();

                while (true)
                {

                    TcpClient clientSocket = serverSocket.AcceptTcpClient();
                    NetworkStream stream = clientSocket.GetStream();


                    byte[] bytes = new byte[256];
                    int length = stream.Read(bytes, 0, bytes.Length);
                    string request = Encoding.ASCII.GetString(bytes, 0, length);
                    Console.WriteLine($"Got request: {request}");

                    string message = $"Length of request: {request.Length}";

                    bytes = Encoding.ASCII.GetBytes(message);
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();
                    Console.WriteLine($"Sent message: {message}");
                    clientSocket.Close();
                }



                serverSocket.Stop();
                Console.WriteLine("Server stopped");
                Console.ReadKey();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
