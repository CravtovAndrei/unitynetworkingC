﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TcpClient client = new TcpClient("127.0.0.1", 7000);
                Console.WriteLine("Client connected.");
                byte[] bytes = new byte[256];

                NetworkStream stream = client.GetStream();

                Console.WriteLine("Enter the request.");
                string request = Console.ReadLine();
                bytes = Encoding.ASCII.GetBytes(request);
                stream.Write(bytes, 0, bytes.Length);
                stream.Flush();
                Console.WriteLine("Client sent request. ");

                byte[] bytesRead = new byte[256];
                int length = stream.Read(bytesRead, 0, bytesRead.Length);
                string answer = Encoding.ASCII.GetString(bytesRead, 0, length);

                Console.WriteLine($"Got message: {answer}");

                client.Close();
                Console.WriteLine("Client done it's work.");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
